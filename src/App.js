import React, { Component } from 'react';
import _ from 'lodash';
import Modal from 'boron/OutlineModal';
import './App.css';

// Todo add music

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      moves: [3],
      userMoves: [],
      currentCellNumber: null,
      isAnimationRunning: false,
      userScore: 0,
      isStrictMode: false,
      isGameRunning: false,
    };

    this.setMove = this.setMove.bind(this);
    this.runAnimation = this.runAnimation.bind(this);
    this.setCurrentCell = this.setCurrentCell.bind(this);
    this.removeActiveCell = this.removeActiveCell.bind(this);
    this.clearUserMoves = this.clearUserMoves.bind(this);
    this.resetGame = this.resetGame.bind(this);
    this.hideModal = this.hideModal.bind(this);
    this.toggleStrictMode = this.toggleStrictMode.bind(this);
  }

  setMove(number) {
    this.setState({
      userMoves: this.state.userMoves.concat(number)
    }, () => {
      this.checkCorrectInput();
    });
  }

  resetGame() {
    this.setState({
      moves: [3],
      userMoves: [],
      userScore: 0,
      isGameRunning: false
    });

    this.refs.modal.hide();
  }

  toggleStrictMode() {
    this.setState({
      isStrictMode: !this.state.isStrictMode,
    });
  }

  checkWin() {
    if (this.state.userScore === 20) {
      alert('Congratulations you won!');
      this.resetGame();
    }
  }

  nextMove() {
    let number = _.random(1, 9);

    // prevent same numbers in next cell
    while (number === this.state.moves[this.state.moves.length - 1]) {
      number = _.random(1, 9);
    }

    this.setState({
      moves: this.state.moves.concat(number),
      userMoves: [],
      userScore: this.state.userScore + 1,
    }, () => {
      this.checkWin();
      this.runAnimation();
    });
  }

  checkCorrectInput() {

    let subArr = this.state.moves.slice(0, this.state.userMoves.length);

    let isEqualArr = _.isEqual(subArr, this.state.userMoves);

    if (isEqualArr && this.state.moves.length === this.state.userMoves.length) {
      this.nextMove();
    } else if (!isEqualArr){

      if (!this.state.isStrictMode) {
        this.showModal();
      } else {
        alert(`Ups, your input is incorrect. Your score ${this.state.userScore}`);
        this.resetGame();
      }
    }

  }

  runAnimation() {

    this.setState({
      isAnimationRunning: true,
      isGameRunning: true,
    });

    this.state.moves.map((currentMove, idx, arr) => {
      setTimeout(() => {

        this.setCurrentCell(currentMove);

        if (arr.length - 1 === idx) {
          this.removeActiveCell();
        }

      }, 1200 * idx);
    });
  }

  setCurrentCell(number) {
    this.setState({
      currentCellNumber: number
    });
  }

  clearUserMoves() {
    this.setState({
      userMoves: []
    });
  }

  removeActiveCell() {
    setTimeout(() => {
      this.setState({
        currentCellNumber: null,
        isAnimationRunning: false
      });
    }, 800)
  }

  showModal(){
    this.refs.modal.show();
  }

  hideModal(){
    this.refs.modal.hide();

    this.clearUserMoves();
    this.runAnimation();

  }

  render() {
    return (
      <div className="App">

        <h1 className="mui--text-headline mui--text-center">Simon Game</h1>

        <div className="user-score">You score: {this.state.userScore}</div>

        <div className={this.state.currentCellNumber ? 'Board active-' + this.state.currentCellNumber : 'Board'}>
          {this.state.isAnimationRunning || !this.state.isGameRunning && <div className="overlay" /> }
          <div className="row">
            <div onClick={this.setMove.bind(this, 1)} className="cell color-1" />
            <div onClick={this.setMove.bind(this, 2)} className="cell color-2" />
            <div onClick={this.setMove.bind(this, 3)} className="cell color-3" />
          </div>
          <div className="row">
            <div onClick={this.setMove.bind(this, 4)} className="cell color-4" />
            <div onClick={this.setMove.bind(this, 5)} className="cell color-5" />
            <div onClick={this.setMove.bind(this, 6)} className="cell color-6" />
          </div>
          <div className="row">
            <div onClick={this.setMove.bind(this, 7)} className="cell color-7" />
            <div onClick={this.setMove.bind(this, 8)} className="cell color-8" />
            <div onClick={this.setMove.bind(this, 9)} className="cell color-9" />
          </div>
        </div>

        {!this.state.isGameRunning &&
          <div className="start-btn">
            <button  onClick={this.runAnimation} className="mui-btn mui-btn--raised mui-btn--primary">Start</button>

            <button onClick={this.toggleStrictMode} className="mui-btn mui-btn--raised mui-btn--accent">
              {!this.state.isStrictMode ? 'Enable strict mode': 'Disable strict mode'}
            </button>
          </div>
        }

        <Modal ref="modal">
          <div className="modal-container">
            <div className="mui--text-headline">
              Ups, your input is incorrect.
            </div>
            <button className="mui-btn mui-btn--raised mui-btn--primary" onClick={this.hideModal}>Try again</button>
            <button className="mui-btn mui-btn--raised mui-btn--danger" onClick={this.resetGame}>Reset game</button>
          </div>
        </Modal>
      </div>
    );
  }
}

export default App;
